#!/usr/bin/env python3

from PIL import Image
from PIL import ImageFilter
import sys

def main():
    if len(sys.argv) <= 2:
        print("usage: denoise.py infile outfile [options]")
        return 1
    im = Image.open(sys.argv[1]).convert("RGB")
    gaussian = [[1,  4,  7,  4, 1], \
                [4, 16, 26, 16, 4], \
                [7, 26, 41, 26, 7], \
                [4, 16, 26, 16, 4], \
                [1,  4,  7,  4, 1]]
    if im.size[0] < 10 or im.size[1] < 10:
        print("image too small")
        return 1
    mode = im.mode
    if mode == "1":
        BLACK = 0
        WHITE = 255
        tupel = False
    elif mode == "RGB":
        BLACK = (0, 0, 0)
        WHITE = (255, 255, 255)
        tupel = True
    elif mode == "RGBA":
        BLACK = (0, 0, 0, 255)
        WHITE = (255, 255, 255, 255)
        tupel = True
        print("mode A")
    else:
        print("NYIE: unsupported mode", mode)
        return 1
    treshhold = 150
    pretreshhold = 150
    if len(sys.argv) > 3:
        treshhold = int(sys.argv[3])
        pretreshhold = treshhold
    if True: # pre threshholding
        im = im.point(lambda i: i>pretreshhold and 255)
    pix = im.load()
    imcopy = im.crop((0, 0, im.size[0], im.size[1]))
    pixcopy = imcopy.load()
    for x in range(2, im.size[0]-2):
        print("line", x, "/", im.size[0])
        for y in range(2, im.size[1]-2):
            p = 0.0
            for j in range(5):
                for k in range(5):
                    if tupel:
                        p += sum(pix[x-2+j, y-2+k]) * gaussian[j][k] / 819
                    else:
                        p += pix[x-2+j, y-2+k] * gaussian[j][k] / 273
            if p<treshhold:
                pixcopy[x, y] = BLACK
            else:
                pixcopy[x, y] = WHITE
    im.paste(imcopy, (0, 0, im.size[0], im.size[1]))
    im.save(sys.argv[2], dpi=(300, 300))

if __name__ == "__main__":
    main()

